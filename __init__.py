# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

bl_info = {
    "name": "Game Utilities",
    "author": "Thomas Larsson",
    "version": (0,1),
    "blender": (2,80,0),
    "location": "View3D > Tools > Game-Utils",
    "description": "Game utilities",
    "warning": "",
    'wiki_url': "http://diffeomorphic.blogspot.com/game-utils/",
    "category": "Import-Export"}

if "bpy" in locals():
    print("Reloading Game Utilities")
    import imp
    imp.reload(utils)
    imp.reload(io_json)
    imp.reload(mesh)
    imp.reload(game)
    imp.reload(action)
    imp.reload(egg)
else:
    print("Loading Game Utilities")
    import bpy
    from . import utils
    from . import io_json
    from . import mesh
    from . import game
    from . import action
    from . import egg

#----------------------------------------------------------
#   Panels
#----------------------------------------------------------

class GUT_PT_Main(bpy.types.Panel):
    bl_category = "Game"
    bl_label = "Game Utilities v %d.%d" % bl_info["version"]
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        self.layout.operator("gut.load_lowpoly_json")
        self.layout.separator()
        self.layout.operator("gut.save_proxy_json")
        self.layout.separator()
        self.layout.operator("gut.fix_action")
        self.layout.operator("gut.purge_bones")
        self.layout.separator()
        self.layout.operator("gut.export_egg")


class GUT_PT_Utilities(bpy.types.Panel):
    bl_category = "Game"
    bl_label = "Utilities"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        ob = context.object
        scn = context.scene
        layout.operator("gut.get_finger")
        layout.separator()
        layout.operator("gut.get_vnums")
        layout.operator("gut.get_enums")
        layout.operator("gut.get_fnums")
        layout.separator()
        layout.operator("gut.save_vnums")
        layout.operator("gut.save_enums")
        layout.operator("gut.save_fnums")
        layout.separator()
        layout.operator("gut.select_vertex")
        layout.operator("gut.select_edge")
        layout.operator("gut.select_face")
        layout.separator()
        layout.operator("gut.list_distances")
        layout.operator("gut.snap_to_closest")
        layout.separator()
        layout.operator("gut.save_lowpoly_json")

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    GUT_PT_Main,
    GUT_PT_Utilities,

    utils.ErrorOperator
]

def register():
    mesh.initialize()
    game.initialize()
    action.initialize()
    egg.initialize()

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    mesh.uninitialize()
    game.uninitialize()
    action.uninitialize()
    egg.uninitialize()

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()

print("Game Utils loaded")

