# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper
from .utils import *

#-------------------------------------------------------------
#   Purge Actions
#-------------------------------------------------------------

class GUT_OT_FixAction(GutOperator, IsArmature):
    bl_idname = "gut.fix_action"
    bl_label = "Fix Action"
    bl_description = "Prepare action for export"
        
    def run(self, context):
        for act,_ in getActiveActions(context.object, ["NLA", "Action"]):
            self.fixAction(act)
            
            
    def fixAction(self, act):            
        marked = []
        first = 1
        last = 1
        for fcu in act.fcurves:
            if len(fcu.keyframe_points) <= 2:
                marked.append(fcu)
        for fcu in marked:
            act.fcurves.remove(fcu)
        for fcu in act.fcurves:
            pt = fcu.keyframe_points[-1]
            if pt.co[0] > last:
                last = int(pt.co[0])
        for fcu in act.fcurves:
            values = [(t, fcu.evaluate(t)) for t in range(first, last+1)]
            for t,y in values:
                fcu.keyframe_points.insert(t, y, options={'NEEDED','FAST'})            


def getActiveActions(ob, use):
    acts = []
    if ob and ob.animation_data:
        if "NLA" in use:
            for track in ob.animation_data.nla_tracks:
                if track.is_solo:
                    for strip in track.strips:
                        acts.append((strip.action, (strip.frame_start, strip.frame_end)))
        if "Action" in use:
            act = ob.animation_data.action
            if act:
                acts.append((act, act.frame_range))
    if acts:
        print(acts)
        return acts
    else:
        raise GutError("No animations found")

#-------------------------------------------------------------
#   Purge Bones
#-------------------------------------------------------------

class GUT_OT_PurgeBones(GutPropsOperator, IsArmature):
    bl_idname = "gut.purge_bones"
    bl_label = "Purge Bones"
    bl_description = "Merge bones without animations to their parents"
        
    deleteUnused : BoolProperty(
        name = "Delete Unused Vertex Groups",
        description = "Delete vertex groups without bones",
        default = False)
                
    def draw(self, context):
        self.layout.prop(self, "deleteUnused")
        
    def run(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        rig = context.object
        self.taken = dict([(bone.name, False) for bone in rig.data.bones])
        for act,_ in getActiveActions(rig, ["NLA", "Action"]):
            self.addTaken(act)
        self.reals = dict([(bone.name, bone.name) for bone in rig.data.bones])
        self.sizes = dict([(bone.name, []) for bone in rig.data.bones])
        roots = [bone.name for bone in rig.data.bones if not bone.parent]
        for root in roots:
            bone = rig.data.bones[root]
            self.findRealBone(bone, root)
        bpy.ops.object.mode_set(mode='EDIT')
        for root in roots:
            eb = rig.data.edit_bones[root]
            self.purgeBone(eb, rig.data.edit_bones)
        for eb in rig.data.edit_bones:
            self.rescaleBone(eb)
        bpy.ops.object.mode_set(mode='OBJECT')
        for ob in rig.children:
            if ob.type == 'MESH':
                self.purgeVertexGroups(ob)
        print("Unused bones purged")
        

    def addTaken(self, act):            
        for fcu in act.fcurves:
            words = fcu.data_path.split('"')
            if words[0] == "pose.bones[":
                self.taken[words[1]] = True

        
    def findRealBone(self, bone, real):
        if self.taken[bone.name]:
            real = bone.name
        else:
            self.reals[bone.name] = real
        self.sizes[real].append(bone.length)
        for child in bone.children:
            self.findRealBone(child, real)


    def purgeBone(self, eb, ebones):
        for child in eb.children:
            self.purgeBone(child, ebones)
        if not self.taken[eb.name]:
            real = ebones[self.reals[eb.name]]
            for child in eb.children:
                child.use_connect = False
                child.parent = real
            ebones.remove(eb)
            del eb


    def rescaleBone(self, eb):
        size = self.sizes[eb.name]
        if len(size) == 2:
            fac = 1 + size[1]/size[0]
            vec = (eb.tail - eb.head)*fac
            eb.tail = eb.head + vec


    def purgeVertexGroups(self, ob):
        weights = {}
        sums = dict([(v.index, 0) for v in ob.data.vertices])
        for vgrp in ob.vertex_groups:
            if vgrp.name in self.reals.keys():
                real = self.reals[vgrp.name]
                if real not in weights.keys():
                    weights[real] = dict([(v.index, 0) for v in ob.data.vertices])
                wts = weights[real]
                for v in ob.data.vertices:
                    for g in v.groups:
                        if g.group == vgrp.index:
                            wts[v.index] += g.weight
                            sums[v.index] += g.weight
                            break
        for wts in weights.values():
            for vn,sum in sums.items():
                if sum != 0:
                    wts[vn] /= sum
        for vgrp in ob.vertex_groups:
            if self.deleteUnused or vgrp.name in self.reals.keys():
                ob.vertex_groups.remove(vgrp)
                del vgrp
        for vgname,wts in weights.items():
            vgrp = ob.vertex_groups.new(name=vgname)
            for vn,w in wts.items():
                if w > 1e-3:
                    vgrp.add([vn], w, 'REPLACE')

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    GUT_OT_FixAction,
    GUT_OT_PurgeBones,
]

def initialize():
    for cls in classes:
        bpy.utils.register_class(cls)


def uninitialize():
    for cls in classes:
        bpy.utils.unregister_class(cls)
