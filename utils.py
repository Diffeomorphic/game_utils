# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

import bpy
import math
from mathutils import Vector

#-------------------------------------------------------------
#   Update
#-------------------------------------------------------------

def updateScene(context):
    depth = context.evaluated_depsgraph_get()
    depth.update()

#-------------------------------------------------------------
#   Coords
#-------------------------------------------------------------

theScale = 1.0

def d2b(v):
    return theScale*Vector((v[0], -v[2], v[1]))

def b2d(v):
    return Vector((v[0], v[2], -v[1]))/theScale

def d2bu(v):
    return Vector((v[0], -v[2], v[1]))

def d2bs(v):
    return Vector((v[0], v[2], v[1]))

D = math.pi/180
R = 180/math.pi

#-------------------------------------------------------------
#   Errors
#-------------------------------------------------------------

theMessage = "No message"
theErrorLines = []

class ErrorOperator(bpy.types.Operator):
    bl_idname = "gut.error"
    bl_label = "Game Utilities"

    def execute(self, context):
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        global theMessage, theErrorLines
        theErrorLines = theMessage.split('\n')
        maxlen = len(self.bl_label)
        for line in theErrorLines:
            if len(line) > maxlen:
                maxlen = len(line)
        width = 20+5*maxlen
        height = 20+5*len(theErrorLines)
        #self.report({'INFO'}, theMessage)
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=width, height=height)

    def draw(self, context):
        global theErrorLines
        for line in theErrorLines:
            self.layout.label(text=line)


class GutError(Exception):

    def __init__(self, value, warning=False):
        global theMessage
        if warning:
            theMessage = "WARNING:\n" + value
        else:
            theMessage = "ERROR:\n" + value
        bpy.ops.gut.error('INVOKE_DEFAULT')

    def __str__(self):
        global theMessage
        return repr(theMessage)

#-------------------------------------------------------------
#   Poll
#-------------------------------------------------------------

class IsMesh:
    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'MESH')


class IsArmature:
    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type == 'ARMATURE')

#-------------------------------------------------------------
#   Execute
#-------------------------------------------------------------

class GutOperator(bpy.types.Operator):
    def execute(self, context):
        try:
            self.run(context)
        except GutError:
            bpy.ops.gut.error('INVOKE_DEFAULT')
        return{'FINISHED'}    

class GutPropsOperator(GutOperator):
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)



