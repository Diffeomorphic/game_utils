# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper
from .utils import *

#-------------------------------------------------------------
#   Fingerprints
#-------------------------------------------------------------

FingerPrints = {
    "19296-38115-18872" : "Genesis",
    "21556-42599-21098" : "Genesis 2 Female",
    #"21556-42599-21098" : "Genesis 2 Male",
    "17418-34326-17000" : "Genesis 3 Female",
    "17246-33982-16828" : "Genesis 3 Male",
    "16556-32882-16368" : "Genesis 8 Female",
    "16384-32538-16196" : "Genesis 8 Male",
}

def getFingerPrint(ob):
    if ob.type == 'MESH':
        return ("%d-%d-%d" % (len(ob.data.vertices), len(ob.data.edges), len(ob.data.polygons)))


def getFingerCharacter(ob):
    finger = getFingerPrint(ob)
    if finger in FingerPrints.keys():
        char = FingerPrints[finger]
    else:
        char = None
    return finger, char


class GUT_OT_GetFinger(bpy.types.Operator, IsMesh):
    bl_idname = "gut.get_finger"
    bl_label = "Get Fingerprint"

    def execute(self, context):
        finger,char = getFingerCharacter(context.object)
        print("%s: %s" % (finger, char))
        return{'FINISHED'}        

#-------------------------------------------------------------
#   Get Numbers
#-------------------------------------------------------------

class GetNumber:
    def getData(self, ob):
        bpy.ops.object.mode_set(mode='OBJECT')
        if self.type == "vertices":
            return [(v.index, v.co) for v in ob.data.vertices if v.select]
        elif self.type == "edges":
            return [(e.index, list(e.vertices)) for e in ob.data.edges if e.select]
        elif self.type == "polygons":
            return [(f.index, list(f.vertices)) for f in ob.data.polygons if f.select]


class ListNumber:
    expand : BoolProperty(name="Expand", default=False)
        
    def draw(self, context):            
        self.layout.prop(self, "expand")

    def execute(self, context):
        data = self.getData(context.object)
        if self.expand:
            for n,x in data:
                print(" %5d: %s" % (n,x))
        else:
            while data:
                print([n for n,_ in data[0:5]])
                data = data[5:]
        return{'FINISHED'}        

    def invoke(self, context, event):
        context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}       
        
    
class GUT_OT_GetVertexNumbers(bpy.types.Operator, IsMesh, GetNumber, ListNumber):
    bl_idname = "gut.get_vnums"
    bl_label = "Get Vertex Numbers"
    type = "vertices"

class GUT_OT_GetEdgeNumbers(bpy.types.Operator, IsMesh, GetNumber, ListNumber):
    bl_idname = "gut.get_enums"
    bl_label = "Get Edge Numbers"
    type = "edges"

class GUT_OT_GetFaceNumbers(bpy.types.Operator, IsMesh, GetNumber, ListNumber):
    bl_idname = "gut.get_fnums"
    bl_label = "Get Face Numbers"
    type = "polygons"

#-------------------------------------------------------------
#   Save numbers
#-------------------------------------------------------------
    
class JsonFile:
    filename_ext = ".json"
    filter_glob : StringProperty(default="*.json", options={'HIDDEN'})
    filepath : StringProperty(
        name="File Path",
        description="Filepath to the .json file",
        maxlen=1024,
        default = "")

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


class SaveNumber(JsonFile, ExportHelper):
    def run(self, context):
        from .io_json import saveJson
        struct = {}
        struct[self.type] = [n for n,x in self.getData(context.object)]
        saveJson(struct, self.filepath)    
        print("%s saved" % self.filepath)
        

class GUT_OT_SaveVertexNumbers(GutOperator, IsMesh, GetNumber, SaveNumber):
    bl_idname = "gut.save_vnums"
    bl_label = "Save Vertex Numbers"
    type = "vertices"

class GUT_OT_SaveEdgeNumbers(GutOperator, IsMesh, GetNumber, SaveNumber):
    bl_idname = "gut.save_enums"
    bl_label = "Save Edge Numbers"
    type = "edges"

class GUT_OT_SaveFaceNumbers(GutOperator, IsMesh, GetNumber, SaveNumber):
    bl_idname = "gut.save_fnums"
    bl_label = "Save Face Numbers"
    type = "polygons"

#-------------------------------------------------------------
#   Select number
#-------------------------------------------------------------

class SelectNumber:
    index : IntProperty(name="Index", default=0, min=0)
        
    def draw(self, context):            
        self.layout.prop(self, "index")

    def run(self, context):
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='OBJECT')
        stuff = getattr(context.object.data, self.type)
        if self.index < len(stuff):
            stuff[self.index].select = True
        else:
            raise GutError("%s %d is out of range" % (self.type.capitalize(), self.index))
        bpy.ops.object.mode_set(mode='EDIT')

    def invoke(self, context, event):
        context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}      

    
class GUT_OT_SelectVertex(GutOperator, IsMesh, SelectNumber):
    bl_idname = "gut.select_vertex"
    bl_label = "Select Vertex"
    type = "vertices"

class GUT_OT_SelectEdge(GutOperator, IsMesh, SelectNumber):
    bl_idname = "gut.select_edge"
    bl_label = "Select Edge"
    type = "edges"

class GUT_OT_SelectFace(GutOperator, IsMesh, SelectNumber):
    bl_idname = "gut.select_face"
    bl_label = "Select Face"
    type = "polygons"
        
#-------------------------------------------------------------
#   Loop over mesh vertices
#-------------------------------------------------------------
        
class MeshLooper:
    def meshLoop(self, context, hum, clo):
        applyTransforms(context, hum)
        applyTransforms(context, clo)
        print("Clothing: %s, Human: %s" % (clo.name, hum.name))
        for cv in clo.data.vertices:
            dists = self.getDistances(cv, hum.data)
            dists.sort()
            self.actOnDistances(cv, hum.data, dists)
    
    def getDistances(self, cv, me):
        return [((hv.co - cv.co).length, hv.index) for hv in me.vertices]
        

def applyTransforms(context, ob):
    context.view_layer.objects.active = ob
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)


def getHumanAndClothing(context):    
    hum = context.object
    clo = None
    for ob in context.view_layer.objects:
        if ob.select_get() and ob != hum:
            clo = ob
            break
    if clo is None:
        raise GutError("Two meshes must be selected")            
    if len(clo.data.vertices) > len(hum.data.vertices):
        return clo,hum
    else:
        return hum,clo


class GUT_OT_ListDistances(GutOperator, IsMesh, MeshLooper):
    bl_idname = "gut.list_distances"
    bl_label = "List Distances"
    bl_description = "List distance between vertices in small mesh to the closest vertex in large mesh"

    def run(self, context):
        hum,clo = getHumanAndClothing(context)
        self.meshLoop(context, hum, clo)

    def actOnDistances(self, cv, me, dists):
        dist,hvn = dists[0]
        print("  ", cv.index, hvn, dist)


class GUT_OT_SnapToClosest(GutOperator, IsMesh, MeshLooper):
    bl_idname = "gut.snap_to_closest"
    bl_label = "Snap To Closest"
    bl_description = "Snap vertices of small mesh to the closest vertex in large mesh"
    bl_options = {'UNDO'}

    def run(self, context):
        hum,clo = getHumanAndClothing(context)
        self.meshLoop(context, hum, clo)
        
    def actOnDistances(self, cv, me, dists):
        dist,hvn = dists[0]
        cv.co = me.vertices[hvn].co
        if cv.index % 100 == 0:
            print("  ", cv.index)
                        
#----------------------------------------------------------
#   Save game character info in json file
#----------------------------------------------------------

def getLowpolyFilepath(char):
    if char is None:
        raise GutError("Character with fingerprint %s is unknown." % finger)
    folder = os.path.dirname(__file__)
    fname = "%s.json" % char.lower().replace(" ", "_")
    return os.path.join(folder, "lowpolys", fname)


class GUT_OT_SaveLowpolyJson(GutOperator, IsMesh, MeshLooper):
    bl_idname = "gut.save_lowpoly_json"
    bl_label = "Save Lowpoly Json"
    bl_description = "Save information about lowpoly mesh in a json file"

    def run(self, context):
        from .io_json import saveJson                
        hum,clo = getHumanAndClothing(context)
        self.meshLoop(context, hum, clo)
        finger,char = getFingerCharacter(hum)
        filepath = getLowpolyFilepath(char)
        struct = {
            "name" : char,
            "fingerprint" : finger,
            "vertices" : self.verts,
            "faces" : [list(f.vertices) for f in clo.data.polygons],
            "uvs" : getUvCoords(clo.data),
        }        
        saveJson(struct, filepath)
        print("%s saved" % filepath)

    def meshLoop(self, context, hum, clo):
        self.verts = []
        MeshLooper.meshLoop(self, context, hum, clo)
                
    def actOnDistances(self, cv, me, dists):
        dist,hvn = dists[0]
        self.verts.append(hvn)
        if cv.index % 100 == 0:
            print("  ", cv.index)


def getUvCoords(me):
    uvlayer = me.uv_layers.active
    m = 0
    uvs = []
    for f in me.polygons:        
        nverts = len(f.vertices)
        fuvs = [tuple(uvlayer.data[m+n].uv) for n in range(nverts)]
        uvs.append(fuvs)
        m += nverts
    return uvs        

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    GUT_OT_GetFinger,
    
    GUT_OT_GetVertexNumbers,
    GUT_OT_GetEdgeNumbers,
    GUT_OT_GetFaceNumbers,
    
    GUT_OT_SaveVertexNumbers,
    GUT_OT_SaveEdgeNumbers,
    GUT_OT_SaveFaceNumbers,
    
    GUT_OT_SelectVertex,
    GUT_OT_SelectEdge,
    GUT_OT_SelectFace,    
    
    GUT_OT_ListDistances,
    GUT_OT_SnapToClosest,
    
    GUT_OT_SaveLowpolyJson,
]

def initialize():
    for cls in classes:
        bpy.utils.register_class(cls)


def uninitialize():
    for cls in classes:
        bpy.utils.unregister_class(cls)
