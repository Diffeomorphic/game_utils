# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

import os
import numpy as np

import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper, ImportHelper
from .utils import *
from .mesh import JsonFile


class GUT_OT_SaveProxyJson(GutOperator, IsMesh, JsonFile, ExportHelper):
    bl_idname = "gut.save_proxy_json"
    bl_label = "Save Game Mesh Info"
    bl_description = "Save information about game proxy mesh in a json file"

    useGroups : BoolProperty(
        name = "Use Vertex Groups",
        description = "Use vertex groups to control fitting",
        default = False)
    
    margin : FloatProperty(
        name = "Margin",
        description = "Only consider human verts within this distance (in meters)",
        default = 0.1)        
    
    def invoke(self, context, event):
        folder = os.path.dirname(__file__)
        ob = context.object
        name = ob.name.lower().replace(" ","_")
        self.filepath = os.path.join(folder, "lowpolys", name+".json")
        return JsonFile.invoke(self, context, event)

    def run(self, context):
        from .mesh import getHumanAndClothing, applyTransforms
        hum,clo = getHumanAndClothing(context)
        applyTransforms(context, hum)
        applyTransforms(context, clo)
        print("Clothing: %s, Human: %s" % (clo.name, hum.name))
        
        self.verts = np.array([cv.co for cv in clo.data.vertices], dtype=float)
        self.nverts = len(clo.data.vertices)
        self.setupTriangles(hum.data)
        self.computeDist()
        self.setupIndices()
        self.getCloGroups(clo)
        self.getHumGroups(hum)
        
        info = dict([(cv.index,None) for cv in clo.data.vertices])
        for gname in self.cloGroups.keys():
            close = self.createSlice(gname)
            weights, offsets = self.findWeights(close)
            self.storeInfo(info, weights, offsets, close)
        self.saveFile(info, hum, clo)
        print("Done")


    def getCloGroups(self, clo):
        self.cloGroups = {}
        if not self.useGroups:
            size = len(clo.data.vertices)
            group = np.ones(size, dtype=bool)
            self.cloGroups["default"] = (group,size)
            return
        
        print("Create clothes groups")
        for vgrp in clo.vertex_groups:
            group = np.zeros(len(clo.data.vertices), dtype=bool)
            size = 0
            for v in clo.data.vertices:
                for g in v.groups:
                    if g.group == vgrp.index:
                        group[v.index] = True
                        size += 1
            self.cloGroups[vgrp.name] = (group,size)

        print("Checking clothes groups")
        taken = dict([(v.index,False) for v in clo.data.vertices])
        doubles = []
        for gname in self.cloGroups.keys():
            group,size = self.cloGroups[gname]
            print("  %s: %d" % (gname, size))
            for v in clo.data.vertices:            
                if group[v.index]:
                    if taken[v.index]:
                        doubles.append(v.index)
                    taken[v.index] = True
        missing = [v.index for v in clo.data.vertices if not taken[v.index]]
        if missing:
            msg = ("The following vertices were not assigned to a group:\n%s" % missing)
            raise GutError(msg)
        if doubles:
            msg = ("The following vertices were assigned to multiple groups:\n%s" % doubles)
            raise GutError(msg)
        print("Clothes groups OK")
                    

    def getHumGroups(self, hum):
        self.humGroups = {}
        if not self.useGroups:
            group = np.ones(len(self.tris), dtype=bool)
            self.humGroups["default"] = group
            return
        
        print("Create human groups")
        for vgrp in hum.vertex_groups:
            if vgrp.name not in self.cloGroups.keys():
                continue
            group = np.zeros(len(self.tris), dtype=bool)
            self.humGroups[vgrp.name] = group
            for tn,tri in enumerate(self.tris):
                for vn in tri:
                    v = hum.data.vertices[vn]
                    for g in v.groups:
                        if g.group == vgrp.index:
                            group[tn] = True
                            break

        for gname in self.cloGroups.keys():
            if gname not in self.humGroups.keys():
                raise GutError('Vertex group "%s"\ndoes not exist in %s' % (gname, hum.name))


    def computeDist(self):
        print("Compute distances")
        r = self.verts[:,np.newaxis,:]
        c = np.sum(self.corners, axis=1)/3
        c = c[np.newaxis,:,:]
        self.dist = np.linalg.norm(r-c, axis=2)


    def createSlice(self, gname):
        clogrp,size = self.cloGroups[gname]
        print("Group %s with %d vertices" % (gname,size))
        humgrp = self.humGroups[gname]
        group = clogrp[:,np.newaxis] * humgrp[np.newaxis,:]
        mindist = np.max(np.min(self.dist, axis=1)[clogrp])
        if self.margin > mindist:
            margin = self.margin
        else:
            margin = 2*mindist
        return (self.dist <= margin) * group


    def setupIndices(self):
        print("Setup indices")
        self.index = np.indices((self.nverts,self.ntris))[0,:,:]
        val = np.indices((self.ntris,self.nverts))[0,:,:]
        self.values = val.T


    def setupTriangles(self, me):
        print("Set up human mesh triangles")
        self.tris = []
        self.corners = []
        for f in me.polygons:
            if len(f.vertices) == 3:
                self.addTri(f.vertices, me)
            elif len(f.vertices) == 4:
                i,j,k,l = f.vertices
                self.addTri((i,j,k), me)
                self.addTri((i,j,l), me)
                self.addTri((i,k,l), me)
                self.addTri((j,k,l), me)
            else:
                raise RuntimeError(
                    "Cannot handle polygons with more than four corners\n" + 
                    "Face %d Verts %s" % (f.index, list(f.vertices)))
        self.ntris = len(self.tris)
        print("Human mesh has %d triangles" % self.ntris)
        self.triangles = np.array(self.tris, dtype=int)
        self.corners = np.array(self.corners, dtype=float)
        print("Numpy arrays created")
        
        
    def addTri(self, tri, me):
        i,j,k = tri
        x = me.vertices[i].co
        y = me.vertices[j].co
        z = me.vertices[k].co
        self.tris.append(tri)
        self.corners.append((x,y,z))


    def findWeights(self, close):     
        r = self.getClose(self.verts, close, 0, 3)
        nclose = r.shape[0]
        print("Find weights from %d combinations" % nclose)   
        x0 = self.getClose(self.corners[:,0,:], close, 1, 3)
        x1 = self.getClose(self.corners[:,1,:], close, 1, 3)
        x2 = self.getClose(self.corners[:,2,:], close, 1, 3)
        e1 = normalize(x1-x0)
        e2 = normalize(x2-x0)
        n = normalize(np.cross(e1, e2, axis=1))
        
        y = r - x0
        k = np.sum(y*n, axis=1)
        z = r - k[:,np.newaxis]*n

        A = np.zeros((nclose, 3, 3), dtype=float)
        A[:,0,:] = 1
        A[:,1,1] = np.sum((x1-x0)*e1, axis=1)
        A[:,1,2] = np.sum((x2-x0)*e1, axis=1)
        A[:,2,1] = np.sum((x1-x0)*e2, axis=1)
        A[:,2,2] = np.sum((x2-x0)*e2, axis=1)

        b = np.zeros((nclose, 3), dtype=float)
        b[:,0] = 1
        b[:,1] = np.sum((z-x0)*e1, axis=1)
        b[:,2] = np.sum((z-x0)*e2, axis=1)
        
        weights = np.linalg.solve(A, b)
        offsets = r-z
        return weights, offsets        


    def getClose(self, x, close, axis, size):                  
        if axis:
            x = x.T
            tiled = np.tile(x, self.nverts)
            tiled = tiled.T           
        else:
            tiled = np.tile(x, self.ntris)
        return tiled.reshape((self.nverts,self.ntris,size))[close]


    def storeInfo(self, info, weights, offsets, close):
        print("Store info")
        index = self.index[close]
        values = self.values[close]
        for n in range(weights.shape[0]):
            vn = index[n]
            tn = values[n]
            tri = self.triangles[tn] 
            if self.better(weights[n], offsets[n], info[vn]):      
                info[vn] = [tri, weights[n], offsets[n]]


    def better(self, wts, offset, info):
        if info is None:
            return True
        bad1 = self.badness(wts)
        bad2 = self.badness(info[1])
        if bad1 < bad2:
            return True
        elif bad1 == bad2:
            return (np.linalg.norm(offset) < np.linalg.norm(info[2]))
        else:
            return False


    def badness(self, wts):        
        bad = 0
        for w in wts:
            if w < 0:
                bad -= w
            elif w > 1:
                bad += (w-1)
        return bad
        

    def saveFile(self, info, hum, clo): 
        from .mesh import getUvCoords, getFingerCharacter
        from .io_json import saveJson  
        print("Save to json file")              
        finger,char = getFingerCharacter(hum)
        info = list(info.items())
        info.sort()  
        if len(info) != len(clo.data.vertices):
            msg = ("No all vertices were matched\nMatched: %d Total: %d\nTry to increase margin" % (len(info), len(clo.data.vertices)))
            raise GutError(msg)
        struct = {
            "name" : hum.name,
            "fingerprint" : finger,
            "info" : [val for key,val in info],
            "faces" : [list(f.vertices) for f in clo.data.polygons],
            "uvs" : getUvCoords(clo.data),
        }        
        saveJson(struct, self.filepath)
        print("%s saved" % self.filepath)

        
def normalize(vec):        
    norm = np.linalg.norm(vec, axis=1)
    return vec/norm[:, np.newaxis]

#----------------------------------------------------------
#   Load game character using info in json file
#----------------------------------------------------------

class GUT_OT_LoadLowpolyJson(GutOperator, IsMesh, JsonFile, ImportHelper):
    bl_idname = "gut.load_lowpoly_json"
    bl_label = "Load Game Mesh"
    bl_description = "Load lowpoly version of the active mesh"
    bl_options = {'UNDO'}

    def run(self, context):
        from .io_json import loadJson
        from .mesh import getFingerCharacter
        hum = context.object
        if not os.path.exists(self.filepath):
            raise GutError('The file "%s"\ndoes not exist' % self.filepath)
        print("Open", self.filepath)
        struct = loadJson(self.filepath)
        for key in ["name", "fingerprint", "info", "faces", "uvs"]:
            if key not in struct.keys():
                raise GutError('The file "%s"\ndoes not describe a game character.' % self.filepath)
        finger,char = getFingerCharacter(hum)
        if struct["fingerprint"] != finger:
            raise GutError("Mesh fingerprint %s does not\nmatch file fingerprint %s" % (finger, struct["fingerprint"]))
        gname = struct["name"] + "_game"
        me = self.buildMesh(gname, hum.data.vertices, struct)
        clo = bpy.data.objects.new(gname, me)
        context.collection.objects.link(clo)
        
    def buildMesh(self, gname, verts, struct):        
        if "vertices" in struct.keys():
            coords = [verts[vn].co for vn in struct["vertices"]]
        elif "info" in struct.keys():
            coords = [w[0]*verts[t[0]].co +
                      w[1]*verts[t[1]].co +
                      w[2]*verts[t[2]].co +
                      Vector(offset)
                      for t,w,offset in struct["info"]]
        
        me = bpy.data.meshes.new(gname)
        me.from_pydata(coords, [], struct["faces"])       
        for f in me.polygons:
            f.use_smooth = True 
        uvlayer = me.uv_layers.new()
        m = 0
        for fuvs in struct["uvs"]:
            for uv in fuvs:
                uvlayer.data[m].uv = uv
                m += 1
        return me

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    GUT_OT_SaveProxyJson,
    GUT_OT_LoadLowpolyJson,
]

def initialize():
    for cls in classes:
        bpy.utils.register_class(cls)


def uninitialize():
    for cls in classes:
        bpy.utils.unregister_class(cls)

