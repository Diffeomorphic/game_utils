# ------------------------------------------------------------------------------
#   BSD 2-Clause License
#   
#   Copyright (c) 2019-2020, Thomas Larsson
#   All rights reserved.
#   
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#   
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#   
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper
from .utils import *

#-------------------------------------------------------------
#   Export Panda egg
#-------------------------------------------------------------
        
class GUT_OT_ExportEgg(GutOperator, ExportHelper):
    bl_idname = "gut.export_egg"
    bl_label = "Export Panda Egg"
    bl_description = "Export Panda egg file"

    filename_ext = ".egg"
    filter_glob : StringProperty(default="*.egg", options={'HIDDEN'})
    filepath : StringProperty(
        name="File Path",
        description="Filepath to the .egg file",
        maxlen=1024,
        default = "")

    useActor : BoolProperty(
        name = "Export Actor",
        description = "Export active armature as an actor",
        default = False
    )
    
    useChildren : EnumProperty(
        items = [("None","None","None"), ("Selected", "Selected", "Selected"), ("All", "All", "All")],
        name = "Export Children",
        description = "Also export all children of the active armature",
        default = "Selected"
    )
    
    useModel : BoolProperty(
        name = "Export Model",
        description = "Export active mesh as a model",
        default = False
    )
    
    useParent : BoolProperty(
        name = "Export Parent",
        description = "Also export the parent armature of the active mesh",
        default = False
    )

    useAnimation : BoolProperty(
        name = "Export Action",
        default = False
    )
    
    useNLA: BoolProperty(
        name = "NLA Strip",
        description = "Export actions in active NLA strip, otherwise use active action",
        default = False
    )
    
    useActiveRange: BoolProperty(
        name = "Active Range",
        description = "Export frames in the active range",
        default = True
    )

    first : IntProperty(
        name = "First Frame",
        default = 1
    )
    
    last : IntProperty(
        name = "Last Frame",
        default = 2
    )
    
    def draw(self, context):
        ob = context.object
        if ob.type == 'MESH':
            self.layout.prop(self, "useModel")
            if self.useModel:
                self.layout.prop(self, "useParent")
        elif ob.type == 'ARMATURE':
            self.layout.prop(self, "useActor")
            if self.useActor:
                self.layout.prop(self, "useChildren")
        
        self.layout.separator()
        self.layout.prop(self, "useAnimation")
        if self.useAnimation:
            self.layout.prop(self, "useNLA")
            self.layout.prop(self, "useActiveRange")
            if not self.useActiveRange:
                self.layout.prop(self, "first")  
                self.layout.prop(self, "last")

   
    @classmethod
    def poll(self, context):
        ob = context.object
        return (ob and ob.type in ['MESH', 'ARMATURE'])
        

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

                
    def run(self, context):
        vly = context.view_layer
        self.copied = []
        self.string = "<CoordinateSystem> { Z-up }\n" 

        meshes = []
        rig = None
        if self.useModel: 
            if self.useParent:
                rig = context.object.parent
            meshes = [ob for ob in vly.objects if ob.select_get()]
        elif self.useActor:
            rig = context.object
            for ob in rig.children:
                if ob.type == 'MESH':
                    if (self.useChildren == 'All' or 
                        (self.useChildren == 'Selected' and ob.select_get())):
                        meshes.append(ob)           

        if meshes:   
            self.verts = {}
            self.faces = {}
            self.vmap = {}
            for ob in meshes:
                self.eggMaterials(ob)
                self.getVertices(ob)
                if rig is None:
                    self.eggModel(ob, "  ")
        if rig:
            self.eggActor(rig)

        if self.useAnimation:
            from .action import getActiveActions
            ob = context.object
            use = ("NLA" if self.useNLA else "Action")
            acts = getActiveActions(ob, use)
            if ob.type == 'MESH':
                pass
            else:
                rig = ob
                self.eggActions(rig, acts)

        with open(self.filepath, "w") as fp:
            fp.write(self.string)
        print("%s written" % self.filepath)


    def eggActor(self, rig):
        rigname = self.nicename(rig.name)
        print("Actor", rigname)
        self.string += ("<Group> %s {\n" % rigname +
                        "  <Dart> { 1 }\n")
        self.meshes = [ob for ob in rig.children if ob.type == 'MESH' and ob.select_get()]                                
        roots = [bone for bone in rig.data.bones if bone.parent is None]        
        for root in roots:
            self.string += self.eggBone(root, "  ")
        for ob in self.meshes:            
            self.eggModel(ob, "  ")
        self.string += "}\n"
            

    def getBoneHead(self, bone, loc):            
        head = bone.head + Vector(loc)
        if bone.parent:
            vec = bone.head_local - bone.parent.head_local
            unit = bone.parent.matrix_local.to_3x3().col[1].normalized()
            head[1] = vec @ unit
        return head
                    

    def eggBone(self, bone, tab):
        mat = bone.matrix.to_4x4()
        mat.col[3][0:3] = self.getBoneHead(bone, (0,0,0))
        string = ("%s<Joint> %s {\n" % (tab, bone.name) +
                  self.eggMatrix(mat, tab+"  "))

        for ob in self.meshes:
            vmap = self.vmap[ob.name]
            if bone.name in ob.vertex_groups.keys():
                obname = self.nicename(ob.name)
                indices = {}
                vgrp = ob.vertex_groups[bone.name]
                for v in ob.data.vertices:
                    for g in v.groups:
                        if g.group == vgrp.index:
                            key = "%.6f" % g.weight
                            if key not in indices.keys():
                                indices[key] = []
                            indices[key] += vmap[v.index]
                for key,idxs in indices.items():
                    if key != "0.000000":
                        ref = " ".join([str(idx) for idx in idxs])
                        string += ("%s  <VertexRef> {%s\n" % (tab, ref) +
                                   "%s    <Scalar> membership { %s }  <Ref> { %s }\n" % (tab, key, obname) +
                                   "%s  }\n" % tab)

        for child in bone.children:
            string += self.eggBone(child, tab+"  ")
        string += ("%s}\n" % tab)
        return string                       


    def eggMatrix(self, mat, tab):
        string = ("%s<Transform> {\n" % tab +
                  "%s  <Matrix4> {\n" % tab)
        for col in mat.col:
            string += ("%s    %.6f %.6f %.6f %.6f\n" % (tab, col[0], col[1], col[2], col[3]))
        string += ("%s  }\n" % tab +
                   "%s}\n" % tab)                                
        return string


    def eggModel(self, ob, tab):
        obname = self.nicename(ob.name)
        print("Model", obname)
        self.string += "%s<Group> %s {\n" % (tab, obname)
        self.string += self.eggMatrix(ob.matrix_world, tab+"  ")
        self.string += "%s\n" % tab
        self.string += ("%s  <VertexPool> %s {\n" % (tab,obname))
        for n,v,uv in self.verts[ob.name]:
            self.string += self.eggVertex(n, v, uv, tab)
        self.string += "%s  }\n\n" % tab

        for f,face in self.faces[ob.name]:
            self.string += self.eggPolygon(f, face, obname, tab)
        self.string += "%s}\n" % tab


    def getVertices(self, ob):
        verts = self.verts[ob.name] = []
        faces = self.faces[ob.name] = []
        vmap = self.vmap[ob.name] = dict([(v.index,[]) for v in ob.data.vertices])
        uvs = dict([(v.index,[]) for v in ob.data.vertices])
        uvlayer = ob.data.uv_layers.active
        m = 0
        for f in ob.data.polygons:
            face = []
            for vn in f.vertices:
                uv = uvlayer.data[m].uv
                old = False
                for vt,uv2 in uvs[vn]:
                    if (uv-uv2).length < -1e-4:
                        face.append(vt)
                        vmap[vn].append(vt)
                        old = True
                        break
                if not old:
                    uvs[vn].append((m,uv))
                    vmap[vn].append(m)         
                    face.append(m)
                    verts.append((m, ob.data.vertices[vn], uv))
                    m += 1
            faces.append((f,face))
    

    def eggVertex(self, n, v, uv, tab):
        return ("%s    <Vertex> %d {%6f %6f %6f \n" % (tab, n, v.co[0], v.co[1], v.co[2]) +
                "%s      <Normal> { %6f %6f %6f }\n" % (tab, v.normal[0], v.normal[1], v.normal[2]) +
                "%s      <UV>  { %6f %6f }\n" % (tab, uv[0], uv[1]) +
                "%s    }\n" % tab)


    def eggPolygon(self, f, face, obname, tab):
        vstring = " ".join([str(vn) for vn in face])
        mname = self.materials[f.material_index]
        string = "%s  <Polygon> {\n" % tab
        for envtype,img in self.textures[mname].items():
            string += ("%s    <TRef> { %s }\n" % (tab, img.name))
        string += ("%s    <MRef> { %s }\n" % (tab, mname) +
                   "%s    <Normal> { %6f %6f %6f }\n" % (tab, f.normal[0], f.normal[1], f.normal[2]) +
                   "%s    <VertexRef> { %s <Ref> { %s }}\n" % (tab, vstring, obname) +
                   "%s  }\n" % tab)
        return string


    def eggMaterials(self, ob):                          
        self.textures = {}
        self.materials = {}
        if len(ob.data.materials) == 0:
            mname = "default"
            self.materials[0] = mname
            self.setupMaterial(mname)
            self.string += self.materialString(mname) + "\n"
            return
        for n,mat in enumerate(ob.data.materials):
            self.materials[n] = mat.name
            self.textures[mat.name] = {}
            self.string += self.eggMaterial(mat)
        for struct in self.textures.values():
            for envtype,img in struct.items():
                print("  IMG", envtype, img)
                self.string += self.eggTexture(envtype, img)
        self.string += "\n"


    def setupMaterial(self, mname):
        self.diffuse = (1,1,1)
        self.specular = (0.5,0.5,0.5)
        self.shininess = 0.5
        self.ambient = (1,1,1)
        self.emit = (0,0,0)
        self.textures[mname] = {}
        

    def eggMaterial(self, mat):
        self.setupMaterial(mat.name)
        for node in mat.node_tree.nodes:
            if node.type == "BSDF_PRINCIPLED":
                self.diffuse = self.getTex(node, "MODULATE", "Base Color", mat)
                self.shininess = self.getTex(node, "GLOSS", "Specular", mat)
            elif node.type == "BSDF_DIFFUSE":
                self.diffuse = self.getTex(node, "MODULATE", "Color", mat)
            elif node.type == "BSDF_GLOSSY":
                self.shininess = self.average(self.getTex(node, "GLOSS", "Color", mat))
            elif node.type == "EEVEE_SPECULAR":
                self.specular = self.getTex(node, "SPECULAR", "Specular", mat)
            elif node.type == "NORMAL_MAP":
                self.normal = self.getTex(node, "NORMAL", "Color", mat)
            elif node.type == "EMISSION":
                self.emit = self.getTex(node, "EMIT", "Color", mat)
        return self.materialString(mat.name)


    def average(self, color):
        return (color[0] + color[1] + color[2])/3
        

    def materialString(self, mname):                        
        return ("<Material> %s {\n" % mname +
                self.eggRGB("diff", self.diffuse) +
                self.eggRGB("spec", self.specular) +
                "  <Scalar> shininess { %.6f }\n" % self.shininess +
                self.eggRGB("amb", self.ambient) +
                self.eggRGB("emit", self.emit) +
                "}\n")


    def getTex(self, node, envtype, slot, mat):
        color = node.inputs[slot].default_value
        for link in mat.node_tree.links:
            if (link.to_node == node and
                link.to_socket.name == slot):
                if link.from_node.type == "TEX_IMAGE":
                    img = link.from_node.image
                    self.textures[mat.name][envtype] = img
        return color                    
        
        
    def eggTexture(self, envtype, img):
        srcpath = bpy.path.abspath(img.filepath)
        filename = os.path.basename(srcpath)
        if filename in self.copied:
            return ""
        self.copied.append(filename)
        folder = os.path.join(os.path.dirname(self.filepath), "tex")
        if not os.path.exists(folder):
            print("Create %s" % folder)
            os.makedirs(folder)
        trgpath = os.path.join(folder, filename)
        print("Copy %s to %s" % (srcpath, trgpath))
        from shutil import copyfile
        copyfile(srcpath, trgpath)
        
        return ("<Texture> %s {\n" % img.name +
                "  \"./tex/%s\"\n" % filename +
                "  <Scalar> envtype { %s }\n" % envtype +
                "  <Scalar> minfilter { LINEAR_MIPMAP_LINEAR }\n" +
                "  <Scalar> magfilter { LINEAR_MIPMAP_LINEAR }\n" +
                "  <Scalar> wrap { REPEAT }\n" +
                "}\n")
                
        
    def eggRGB(self, prefix, color):
        return ("  <Scalar> %sr { %.6f }\n" % (prefix, color[0])+
                "  <Scalar> %sg { %.6f }\n" % (prefix, color[1]) +
                "  <Scalar> %sb { %.6f }\n" % (prefix, color[2]))

        
    def nicename(self, name):
        return name.replace(" ", "_")


    def eggActions(self, rig, acts):
        self.setActiveRange(acts)    
        rname = self.nicename(rig.name)
        self.string += ("<Table> {\n" +
                        "  <Bundle> %s {\n" % rname +
                        "    <Table> \"<skeleton>\" {\n")
        npoints = self.last - self.first + 1
        self.anims = {}
        for bone in rig.data.bones:
            self.anims[bone.name] = [Keyframe() for n in range(npoints)]
        for act,frameRange in acts:
            self.addAnims(act, frameRange)
        roots = [pb for pb in rig.pose.bones if pb.parent is None]
        for root in roots:
            self.eggBoneAnim(root, rig, "      ")
        self.string += ("    }\n" +
                        "  }\n" +
                        "}\n")


    def setActiveRange(self, acts):
        if not self.useActiveRange:
            return
        self.first = 1
        self.last = -10000
        for _,frameRange in acts:
            first,last = frameRange
            if first < self.first:
                self.first = int(first)
            if last > self.last:
                self.last = int(last)

            
    def addAnims(self, act, frameRange): 
        start = int(round(frameRange[0]))
        end = int(round(frameRange[1]))
        if start > self.last or end < self.first:
            return
        first = max(start, self.first)
        last = min(end, self.last)        
        for fcu in act.fcurves:
            words = fcu.data_path.split('"')
            if words[0] == "pose.bones[":
                channel = words[-1].rsplit(".")[-1]
                idx = fcu.array_index                
                anim = self.anims[words[1]]
                for t in range(first, last+1):
                    anim[t-self.first].set(channel, idx, fcu.evaluate(t-start+1))


    def eggBoneAnim(self, pb, rig, tab):
        from mathutils import Quaternion, Vector, Euler, Matrix
        anim = self.anims[pb.name]
        if anim:
            self.string += ("%s<Table> %s {\n" % (tab, pb.name) +
                            "%s  <Xfm$Anim> xform {\n" % tab +
                            "%s    <Scalar> order { sprht }\n" % tab +
                            "%s    <Scalar> fps { 24 }\n" % tab +
                            "%s    <Scalar> contents { ijkprhxyz }\n" % tab +
                            "%s    <V> {\n" % tab)
            for frame in anim:
                s = frame.scale
                if pb.rotation_mode == 'QUATERNION':
                    q = Quaternion(frame.rotation)
                else:
                    e = Euler(frame.rotation[1:4], pb.rotation_mode)
                    q = e.to_quaternion()
                mat = pb.bone.matrix @ q.to_matrix()
                if pb.parent is None:
                    mat = rig.matrix_world.to_3x3() @ mat
                r = Vector(mat.to_euler('XYZ'))*R
                l = self.getBoneHead(pb.bone, frame.location)
                self.string += (
                    "%s      %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n" % 
                    (tab, s[0], s[1], s[2], r[0], r[1], r[2], l[0], l[1], l[2]))
            self.string +=("%s    }\n" % tab +
                           "%s  }\n" % tab)
        for child in pb.children:
            self.eggBoneAnim(child, rig, tab + "  ")                           
        self.string +=("%s}\n" % tab)


class Keyframe:
    def __init__(self):
        from mathutils import Vector
        self.scale = Vector((1,1,1))
        self.rotation = Vector((1,0,0,0))
        self.location = Vector((0,0,0))


    def set(self, channel, idx, y):
        if channel == "scale":
            self.scale[idx] = y
        elif channel == "location":
            self.location[idx] = y
        elif channel == "rotation_quaternion":
            self.rotation[idx] = y
        elif channel == "rotation_euler":
            self.rotation[idx+1] = y
            
#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    
    GUT_OT_ExportEgg,
]

def initialize():
    for cls in classes:
        bpy.utils.register_class(cls)


def uninitialize():
    for cls in classes:
        bpy.utils.unregister_class(cls)
        
